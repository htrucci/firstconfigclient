package com.htrucci.firstconfigclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstconfigclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstconfigclientApplication.class, args);
	}
}
