package com.htrucci.firstconfigclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class TestController {

	@Value("${originairports}")
//	@Value("${orginairports:Hello default}")
	private String originAirportShutdownList;


	@RequestMapping(value="/get", method = RequestMethod.GET)
	String search(){
		System.out.println("originairports.shutdown::::"+originAirportShutdownList);


		return originAirportShutdownList;
	}
}
